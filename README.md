**This script is used to analyze docker containers**
Applying forensics to containers

Container (Docker) security threats:
- Container resource abuse
- Container breakouts [CVE-2016-5195, CVE-2017-5123, CVE-2014-9357]
- Leverage as persistence and lateral movement
- Container image authenticity, Source poisoning (bad public images)
- Exposed Host Filesystem
- Network Connections
- Docker credentials and secrets, Creds in Environment

Usage: `sudo ./analyze_container <container_id>`

Some information to know about Containers
There are four basic about Conainers:
-Containers are Processes
-Images are Tar Archives (with some json configs)
-Container Registries are file servers to share images
-Containers are controlled by CGroup
-Containers communicate by namespaces

 In order to run container you need:
 - Container Engine
 - Container Runtime
 - Linux Kernel

**Container Engine**

API or CLI tool for building or running containers. Ex: *Docker, Podman, Buildah, rkt, and CRI-O*. A container engine accepts user inputs, pulls container images, creates some metadata describing how to run the container, then passes this information to a container Runtime.

**Container Runtime**

A container runtime is a small tool that expects to be handed two things - a directory often called a root filesystem (or *rootfs*), and some metadata called *config.json* (or spec file). The most common runtime *runc*.

**Linux Kernel**

The kernel is responsible for the resource management. The container runtime talks to the kernel to create the new container with *clone()* function. The runtime also handles talking to the kernel to configure things like cgroups, SELinux, and SECCOMP

**Explanation of steps in scrip:**
1. Taking snapshot of container:
docker commit allows you to save a snapshot of your container as a docker image so you can analise it later
`docker commit <docker_container_id> <some_name>:versin_tag`
`docker save some_name > some_name.ta`r
`tar -xvf some_name.tar`
`cat manifest.json`
`tar -xvf 2d0ffa165f26b68389c0f6dd474cd9428fc2a72dbc14766d60b777c0a00a005d/layer.tar`
(docker export/import does not save history)

Processes:

To see information about the process including the PID (Process ID) and PPID (Parent Process ID):
`docker top <container_id>`

To see who is the Parent Process:
`ps aux | grep <PPID>`

The command pstree will list all of the sub processes. See the Docker process tree using
`pstree -c -p -A $(pgrep dockerd)`
`pstree -c -p -A $(pgrep containerd)`

Process Directory:

The configuration for each process is defined within the /proc directory. If you know the process ID, then you can identify the configuration directory.

Each process has it's own configuration and security settings defined within different files.
`ls /proc/<PID>`
`ls /proc/<PPID>`

Command `cat /proc/<PID>/environ`
equal to:
`docker exec -it <container_id> env`
Security Use Case: Creds in Environment Vars

Namespaces:
The concept of namespaces is to limit what processes can see and access certain parts of the system, such as other network interfaces or processes.

When a container is started, the container runtime, such as Docker, will create new namespaces to sandbox the process. By running a process in it's own Pid namespace, it will look like it's the only process on the system.

The available namespaces are:
- Mount (mnt)
- Process ID (pid)
- Network (net)
- Interprocess Communication (ipc)
- UTS (hostnames)
- User ID (user)
- Control group (cgroup)
Command to list all the namespaces:
ls -lha /proc/<PID>/ns/

Security Use Case: Network Connections
(If net namespace for both processes (both containers) points to the same location, they are connected)


Chroot?

Cgroups (Control Groups)
CGroups limit the amount of resources a process can consume. These cgroups are values defined in particular files within the /proc directory.

To see the mappings, run the command:
`cat /proc/<PID>/cgroup`

One of the properties of Docker is the ability to control memory limits. This is done via a cgroup setting.
`docker stats <docker_container_name> --no-stream`
The memory quotes are stored in a file called memory.limit_in_bytes.

By writing to the file, we can change the limit limits of a process.
`/sys/fs/cgroup/memory/docker/<PID>/memory.limit_in_bytes`


Seccomp / AppArmor

All actions with Linux is done via syscalls. The kernel has 330 system calls that perform operations such as read files, close handles and check access rights. All applications use a combination of these system calls to perform the required operations.

AppArmor is a application defined profile that describes which parts of the system a process can access.
Seccomp provides the ability to limit which system calls can be made, blocking aspects such as installing Kernel Modules or changing the file permissions.

It's possible to view the current AppArmor profile assigned to a process via:
`cat /proc/<PID>/attr/apparmor/current`
docker-default (enforce)

The status of SecComp is also defined within a file.

`cat /proc/<PID>/status`

`cat /proc/<PID>/status | grep Seccomp`

The flag meaning are: 0: disabled 1: strict 2: filtering


Capabilities

Capabilities are groupings about what a process or user has permission to do. These Capabilities might cover multiple system calls or actions, such as changing the system time or hostname.

The status file also containers the Capabilities flag. A process can drop as many Capabilities as possible to ensure it's secure.

`cat /proc/<PID>/status | grep ^Cap`
The flags are stored as a bitmask that can be decoded with capsh

`capsh --decode=00000000a80425fb`
0x00000000a80425fb=cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap

Capabilities of a container run as root
Capability	Permitted Operation
CAP_AUDIT_WRITE	Write records to the kernel’s auditing log
CAP_CHOWN (Change owner)	Make arbitrary changes to file UIDs and GIDs; change the owner and group of files, directories, and links
CAP_DAC_OVERRIDE (Discretionary access control)
Bypass a file’s read, write, and execute permission checks

CAP_FOWNER	Bypass permission checks on operations that normally require the file system UID of the process to match the UID of the file, excluding checks which are covered by CAP_DAC_OVERRIDE and CAP_DAC_READ_SEARCH
CAP_FSETID	Will not clear set-user-ID and set-group-ID mode bits even when a file is changed
CAP_KILL	Bypass permission checks for sending signals
CAP_MKNOD	Create special files
CAP_NET_BIND_SERVICE	Bind a socket to internet domain privileged ports (port numbers below 1024)
CAP_NET_RAW	Use RAW and PACKET sockets and binds to any address for transparent proxying
CAP_SETGID


Make arbitrary manipulations of process GIDs and supplementary GID list
CAP_SETPCAP	If file capabilities are supported (i.e., since Linux 2.6.24): add any capability from the calling thread’s bounding set to its inheritable set; drop capabilities from the bounding set; make changes to the secure bits flags.


If file capabilities are not supported (i.e., kernels before Linux 2.6.24): grant or remove any capability in the caller’s permitted capability set to or from any other process

CAP_SETUID


Make arbitrary manipulations of process UIDs, forge UID when passing socket credentials via UNIX domain sockets, and write a user ID mapping in a user namespace
CAP_SYS_CHROOT	Use chroot and changes namespaces using setns.

Security Use Case: Container Breakout
`docker run -t -i --privileged ubuntu bash`
