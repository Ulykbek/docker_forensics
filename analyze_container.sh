#!/bin/bash

# Warning
# This script is used to analyze docker containers
# Usage: ./analyze_container <container_id>

CONTAINER_ID=$1
ROOT_UID=0
E_NOTROOT=87

# Run as a root
if [ "$UID" -ne "$ROOT_UID" ]
then
  echo -e "\033[1;31m Must be root to run this script.\033[0m"
  exit $E_NOTROOT
fi

if [ -z "$1" ]; then
	echo -e "\033[1;31m Usage: '$0 <docker_container_id>'\033[0m"
	exit 1
fi

clear

# Save a snapshot of a target container
echo -e "\033[1;31m----------Taking snapshot image of a target container----------\033[0m"
docker commit $CONTAINER_ID snapshot:version_1
echo -e "\033[1;31m----------Making tar archive of a snapshot image as snapshot.tar----------\033[0m"
docker save snapshot > snapshot.tar
echo -e "\033[1;31m----------Extracting snapshot.tar----------\033[0m"
tar -xvf snapshot.tar
echo -e "\033[1;31m----------Printing a manifest.json----------\033[0m"
cat manifest.json

echo "\033[1;31m----------Listing docker images to check snapshot is taken----------\033[0m"
docker images | grep snapshot

echo -e "\033[1;31m----------Docker stats----------\033[0m"
docker stats $CONTAINER_ID --no-stream

echo -e "\033[1;31m----------Printing PIDs of a target container----------\033[0m"
pid_arr=($(docker top $CONTAINER_ID | awk '{print $2}' | tail -n+2))
# loop through every element in the array
for i in "${pid_arr[@]}"
do
   :
  echo $i
  echo -e "\033[1;31m----------Check Environment Vars (For Creds)----------\033[0m"
  cat /proc/$i/environ
  echo "\n"
  echo -e "\033[1;31m----------List all the namespaces----------\033[0m"
  ls -lha /proc/$i/ns/
  echo -e "\033[1;31m----------List cgroup----------\033[0m"
  cat /proc/<$i/cgroup
  echo -e "\033[1;31m----------AppArmor----------\033[0m"
  cat /proc/<$i/attr/current
  echo -e "\033[1;31m----------Seccomp----------\033[0m"
  cat /proc/$i/status | grep Seccomp
  echo -e "\033[1;33m----------The flag meaning are: 0: disabled 1: strict 2: filtering----------\033[0m"
  echo -e "\033[1;31m----------Capabilities----------\033[0m"
  capsh --decode=$(cat /proc/$i/status | grep ^Cap | awk '{print $2}' | head -n 1)
  echo -e "\033[1;33m----------Check if Container is Privileged----------\033[0m"
  docker inspect $CONTAINER_ID | grep "Privileged"

done


echo -e "\033[1;31m----------Printing Parent PIDs of a target container----------\033[0m"
ppid_arr=($(docker top $CONTAINER_ID | awk '{print $3}' | tail -n+2))
# loop through every element in the array
for i in "${ppid_arr[@]}"
do
   :
  echo $i
  echo -e "\033[1;31m----------Printing Parent Process of a container----------\033[0m"
  ps aux | grep $i
done

#echo "-----All container ids to the array-----"
#id_arr=($(docker ps -a | awk '{print $1}' | tail -n+2))
#for i in "${id_arr[@]}"
#do
#	:
#	echo $i
#done
